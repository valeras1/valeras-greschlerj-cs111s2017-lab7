/* ***********************************************************************
The work we are submitting is a result of our own thinking and efforts
Sam Valera and Josh Greschler
CMPSC 111 Spring 2017
Lab 7
Date 3/3/17

Pourpse: to accept values from CommandLineGeometer, perform calculations, and return values of geometric properties such as surface area.
********************************************************************* */

//imports the math class which allows us to perform more complex calculations
import java.lang.Math;


public class GeometricCalculator {

   //method calculateSphereVolume is called by CommandLineGeometer, which gives it a radius.
    public static double calculateSphereVolume(double radius) {
  
	//uses the input of radius, calculates the volume of the corresponding sphere
	double volume1, fraction;
	fraction = (1.333333);
        volume1 = fraction * (Math.PI) * radius * radius * radius;
        //returns the calculated value to the CommandLineGeometer
	return volume1;

    }
	//all following methods follow the same basic structure presented above.

    public static double calculateTriangleArea(double a, double b, double c) {

        double area, per;
	per = (a + b + c) / 2;
	area = Math.sqrt(per * (per - a) * (per - b) * (per - c));
        return area;
    }

    public static double calculateCylinderVolume(double radius, double height) {
        double volume;
        volume = (Math.PI) * radius * radius * height;
        return volume;
    }
    public static double calculateSphereSurfaceArea(double radius) {
	
	double surfacearea;
	surfacearea = 4 * (Math.PI) * radius * radius;
	return surfacearea;
}
    public static double calculateCylinderSurfaceArea(double radius, double height) {
	
	double surfacearea;
	surfacearea = 2 * (Math.PI) * radius * height;
	return surfacearea;
}
}
